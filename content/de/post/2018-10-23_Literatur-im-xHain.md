---
title: "Literatur im xHain"
slug: ""
thumbnailImage: /img/books.jpg
heroImage: /img/books.jpg
heroCaption: ""
date: 2018-10-23
categories:
- news
- events
tags:
- xhain
- events
keywords:
- xhain
type: blog
aliases:
- /de/2018/10/literatur-im-xhain
---

Vorzustellen braucht man sie nicht - in Zeiten des Digitalen lesen <a href="http://www.jankomarklein.de/">Janko Marklein</a> und <a href="https://de.wikipedia.org/wiki/Lorenz_Just">Lorenz Just</a> im X-Hain aus ihrer Literatur eine spontane Anthologie, die Auszüge aus Romanen wie "Florian Berg ist sterblich" und digital veröffentlichte Texte als einstündiges Ganzes fassen wird. 

Los geht's am Freitag den 26.10.2018 um 20:00 Uhr. Eintritt frei!