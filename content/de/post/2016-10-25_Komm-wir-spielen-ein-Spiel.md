---
title: "Komm, wir spielen ein Spiel!"
slug: ""
thumbnailImage: /img/boardgame.jpg
heroImage: /img/boardgame.jpg
heroCaption: "Maarten van den Heuvel via [Unsplash](https://unsplash.com/photos/_pc8aMbI9UQ) ([CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.de))"
date: 2016-10-25
categories:
- news
- events
tags:
- xhain
- events
keywords:
- xhain
type: blog
aliases:
- /de/2016/10/komm-wir-spielen-ein-spiel
---

Lange erwartet, nun geht's endlich los:

Level 16-10 (Spieleabend): 28.10.

Ab diese Woche findet an jedem letzten Freitag im Monat findet ein Spieleabend für Nerdspiele
statt. Der xHain ist offen ab 20h00, Spielstart ist um 21h00.

<!--more-->

Das erste Spiel auf dem Programm ist <a href="https://boardgamegeek.com/boardgame/18/robo-rally">Robo Rallye</a>.Hier muss man mit Steuerkärtchen Programme für Roboter schreiben, um diese möglichst schnell durch einen Parcours in einer Fabrikhalle mit Förderbändern, Lasern, Stampfern und Löchern zu manövrieren.

Verwendet wird die alte Version mit den Metallfiguren.

Die Spiele für die weiteren Level YY-MM-Abende werden per <a href="www.prefr.org">Prefr</a>
festgelegt.
