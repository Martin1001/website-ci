---
title: Bakiwi Robotik-Bastelworkshop
thumbnailImage: /img/bakiwi.jpg
heroImage:  /img/bakiwi.jpg
slug: ""
heroCaption: ""
date: 2019-06-13
categories:
- news
- events
tags:
- xhain
type: blog
---

Komm zum Bakiwi-Robotikworkshop, baue dir einen kleinen Laufroboter und lass ihn gegen andere antreten. Wir wollen mit euch kleine Roboter bauen und zwar den [Bakiwi](https://github.com/ku3i/Bakiwi). Das Zusammenbauen des Bausatzes erfordert ein wenig Lötarbeit und etwas Geduld, ist aber sehr geeignet für Anfänger*innen, die sich das erste Mal an einem Robotik-Projekt versuchen wollen. Es sind keine Vorkenntnisse nötig. Matthias Kubisch, der auch die Platine entworfen und den Bausatz zusammengestellt hat, leitet den Workshop und wird euch helfen euren Bakiwi zum Zappeln zu bringen.

Stattfinden wird der Workshop am 27.10.2019 von 13:00 bis 17:00 Uhr. 
Für jeden der Workshops können sich bis zu 8 Teilnehmer*innen anmelden. Ihr könnt auch im Zweier-Team zusammen basteln, z.B. 1 Elter + 1 Kind.

Die Kosten für den Workshop inklusive Bausatz betragen 40€.

Zur Individualisierung Eures Roboters bringt doch einen kleinen Beutel mit z.B. Bastelmaterial, Elektroschrott, Lego etc. mit

<form action="https://formspree.io/xhain_hack_makespace@posteo.de"
      method="POST">
    <label for="email">E-Mail:
    	<input type="email" name="_replyto" title="E-Mail" required>
    </label><br>
    <label>Zum Workshop anmelden
    	<input type="submit" value="Ja, bitte" style="background:#408e27">
	</label><br>
</form>
