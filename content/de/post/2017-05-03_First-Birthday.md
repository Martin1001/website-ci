---
title: "Ein ganzes Jahr ist rum. Zeit für Party!"
slug: ""
thumbnailImage: /img/birthday.jpg
heroImage: /img/birthday.jpg
heroCaption: "Rich Helmer via [Unsplash](https://unsplash.com/search/birthday?photo=8gNG1eorhpM) ([CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.de))"
date: 2017-05-03
categories:
- news
- events
tags:
- xhain
- events
keywords:
- xhain
type: blog
aliases:
- /de/2017/05/first-birthday
---

Puh, so schnell kann's gehen: schon ist das Jahr vorbei und der xHain wird 1!<br>
Es ist viel passiert, jede Menge beeindruckende Dinge entstanden ud es hat sich eine echt tolle Community gebildet.<br> Und wie es sich gehört, wollen wir dieses Jubiläum mit Euch ordentlich begehen.<br>
Darum seid ihr herzlichst eingeladen zur Geburtstagsparty!<br><br>
Wann?<br>Am Samstag, den 13. Mai ab 20 Uhr<br>
Wo?<br>Natürlich im xHain<br>
Was geht?<br>Getränke, Musik, Tanz und tolle Leute<br>
Wieviel?<br>Eintritt frei, über Mitbringsel und Spenden wird sich aber sehr gefreut.<br>

