---
comments: false
date: 2017-03-30
showDate: false
showPagination: false
showSocial: false
showTags: false
title: About us
showMap: true
---

To have a local place for exchanges, mutual learning and fun: that’s the vision for xHain.
During the day: a place where projects and school groups can learn.
At night: a place where hackers and makers can meet.
At other times: a conference room or a temporary desk for Friedrichshainers with no office to go to.
This – or something similar – is what founder Felix hopes xHain turns out to be.

## Who It’s For
We want to create a place where makers, people who are interested in tech, and (society) hackers feel comfortable.
You’re welcome no matter who you are or what you do – whether you have a job or not, whether you’re a trainee or a student, a single parent or a senior citizen.

xHain is a non-smoking place. People who want to smoke can do so outside.
If you’re sexist, racist, homophobic or an asshole, please don’t bother coming.
